package es.cic.finalback.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.cic.finalback.model.Tipo;

@Repository
public interface TipoRepository extends JpaRepository<Tipo, Long> {

	Tipo findByCodigo(Integer codigo);
}
