package es.cic.finalback.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.cic.finalback.model.ElementoVial;

@Repository
public interface ElementoVialRepository extends JpaRepository<ElementoVial, Long> {

	ElementoVial findByCodigo(Integer codigo);
	
	List<ElementoVial> findByTipoId(Long idTipo);
}
