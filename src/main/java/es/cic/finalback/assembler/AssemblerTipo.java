package es.cic.finalback.assembler;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.cic.finalback.dto.TipoDTO;
import es.cic.finalback.model.Tipo;
import es.cic.finalback.repository.TipoRepository;
import jakarta.persistence.PrePersist;

@Component
public class AssemblerTipo implements Assembler<Tipo, TipoDTO> {
	
	@Autowired
	TipoRepository repo;

	@Override
	public TipoDTO toDto(Tipo entity) {
		TipoDTO dto = new TipoDTO();
		dto.setId(entity.getId());
		dto.setCodigo(entity.getCodigo());
		dto.setNombre(entity.getNombre());
		dto.setDescripcion(entity.getDescripcion());
		dto.setFechaAlta(entity.getFechaAlta());
		dto.setFechaModificacion(entity.getFechaModificacion());
		dto.setDisponible(entity.isDisponible());
		dto.setSenial(entity.isSenial());
		return dto;
	}

	@PrePersist
	public int generateUniqueCode() {
		Random random = new Random();
		int min = 1000;
		int max = 9999;
		return random.nextInt(max - min + 1) + min;
	}

	@Override
	public Tipo toEntity(TipoDTO dto) {
		Tipo entity = new Tipo();
		int codigo = generateUniqueCode();
		if(repo.findByCodigo(codigo)==null){
			entity.setCodigo(codigo);
		}
		entity.setNombre(dto.getNombre());
		entity.setDescripcion(dto.getDescripcion());
		entity.setFechaAlta(dto.getFechaAlta());
		entity.setFechaModificacion(dto.getFechaModificacion());
		entity.setDisponible(dto.isDisponible());
		entity.setSenial(dto.isSenial());
		return entity;
	}

	@Override
	public List<TipoDTO> toDto(List<Tipo> entities) {
		return entities.stream().map(r -> toDto(r)).collect(Collectors.toList());
	}

	@Override
	public List<Tipo> toEntity(List<TipoDTO> dtos) {
		return dtos.stream().map(r -> toEntity(r)).collect(Collectors.toList());
	}

}
