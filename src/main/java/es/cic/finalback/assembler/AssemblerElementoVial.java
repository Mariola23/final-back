package es.cic.finalback.assembler;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.cic.finalback.dto.ElementoVialDTO;
import es.cic.finalback.model.ElementoVial;
import es.cic.finalback.repository.ElementoVialRepository;
import es.cic.finalback.repository.TipoRepository;
import jakarta.persistence.PrePersist;

@Component
public class AssemblerElementoVial implements Assembler<ElementoVial, ElementoVialDTO> {

	@Autowired
	ElementoVialRepository elementoRepo;
	
	@Autowired
	TipoRepository tipoRepo;

	@Override
	public ElementoVialDTO toDto(ElementoVial entity) {
		ElementoVialDTO dto = new ElementoVialDTO();
		dto.setId(entity.getId());
		dto.setFk_tipo(entity.getTipo().getId());
		dto.setCodigo(entity.getCodigo());
		dto.setLatitud(entity.getLatitud());
		dto.setLongitud(entity.getLongitud());
		dto.setAltitud(entity.getAltitud());
		dto.setFechaInstalacion(entity.getFechaInstalacion());
		dto.setFechaRetirada(entity.getFechaRetirada());
		dto.setInstalado(entity.isInstalado());
		return dto;
	}
	
	@PrePersist
	public int generateUniqueCode() {
		Random random = new Random();
		int min = 1000;
		int max = 9999;
		return random.nextInt(max - min + 1) + min;
	}

	@Override
	public ElementoVial toEntity(ElementoVialDTO dto) {
		ElementoVial entity = new ElementoVial();
		int codigo = generateUniqueCode();
		if(elementoRepo.findByCodigo(codigo)==null){
			entity.setCodigo(codigo);
		}
		entity.setTipo(tipoRepo.findById(dto.getFk_tipo()).get());
		entity.setLatitud(dto.getLatitud());
		entity.setLongitud(dto.getLongitud());
		entity.setAltitud(dto.getAltitud());
		entity.setFechaInstalacion(dto.getFechaInstalacion());
		entity.setFechaRetirada(dto.getFechaRetirada());
		entity.setInstalado(dto.isInstalado());
		return entity;
	}

	@Override
	public List<ElementoVialDTO> toDto(List<ElementoVial> entities) {
		return entities.stream().map(r -> toDto(r)).collect(Collectors.toList());
	}

	@Override
	public List<ElementoVial> toEntity(List<ElementoVialDTO> dtos) {
		return dtos.stream().map(r -> toEntity(r)).collect(Collectors.toList());
	}

}
