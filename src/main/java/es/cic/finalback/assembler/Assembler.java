package es.cic.finalback.assembler;

import java.util.List;

public interface Assembler <E,DTO>{

	DTO toDto(E entity);

	E toEntity(DTO dto);

	List<DTO> toDto(List<E> entities);

	List<E> toEntity(List<DTO> dtos);
}
