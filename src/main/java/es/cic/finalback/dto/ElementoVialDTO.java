package es.cic.finalback.dto;

import java.time.LocalDate;

public class ElementoVialDTO {
	private Long id;
	private Long fk_tipo;
	private Integer codigo;
	private Float latitud;
	private Float longitud;
	private Float altitud;
	private LocalDate fechaInstalacion;
	private LocalDate fechaRetirada;
	private boolean instalado;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getFk_tipo() {
		return fk_tipo;
	}

	public void setFk_tipo(Long fk_tipo) {
		this.fk_tipo = fk_tipo;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public Float getLatitud() {
		return latitud;
	}

	public void setLatitud(Float latitud) {
		this.latitud = latitud;
	}

	public Float getLongitud() {
		return longitud;
	}

	public void setLongitud(Float longitud) {
		this.longitud = longitud;
	}

	public Float getAltitud() {
		return altitud;
	}

	public void setAltitud(Float altitud) {
		this.altitud = altitud;
	}

	public LocalDate getFechaInstalacion() {
		return fechaInstalacion;
	}

	public void setFechaInstalacion(LocalDate fechaInstalacion) {
		this.fechaInstalacion = fechaInstalacion;
	}

	public LocalDate getFechaRetirada() {
		return fechaRetirada;
	}

	public void setFechaRetirada(LocalDate fechaRetirada) {
		this.fechaRetirada = fechaRetirada;
	}

	public boolean isInstalado() {
		return instalado;
	}

	public void setInstalado(boolean instalado) {
		this.instalado = instalado;
	}

}
