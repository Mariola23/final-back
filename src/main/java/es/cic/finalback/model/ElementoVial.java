package es.cic.finalback.model;

import java.time.LocalDate;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.ForeignKey;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "elemento")
public class ElementoVial {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(unique = true)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer codigo;

	@ManyToOne
	@JoinColumn(name = "fk_tipo", nullable = false, foreignKey = @ForeignKey(name = "fk_tipo"))
	private Tipo tipo;

	@Column(nullable = false)
	private Float longitud;

	@Column(nullable = false)
	private Float latitud;

	@Column(nullable = false)
	private Float altitud;

	@Column(nullable = false)
	private LocalDate fechaInstalacion;

	private LocalDate fechaRetirada;

	private boolean instalado;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}

	public Float getLongitud() {
		return longitud;
	}

	public void setLongitud(Float longitud) {
		this.longitud = longitud;
	}

	public Float getLatitud() {
		return latitud;
	}

	public void setLatitud(Float latitud) {
		this.latitud = latitud;
	}

	public Float getAltitud() {
		return altitud;
	}

	public void setAltitud(Float altitud) {
		this.altitud = altitud;
	}

	public LocalDate getFechaInstalacion() {
		return fechaInstalacion;
	}

	public void setFechaInstalacion(LocalDate fechaInstalacion) {
		this.fechaInstalacion = fechaInstalacion;
	}

	public LocalDate getFechaRetirada() {
		return fechaRetirada;
	}

	public void setFechaRetirada(LocalDate fechaRetirada) {
		this.fechaRetirada = fechaRetirada;
	}

	public boolean isInstalado() {
		return instalado;
	}

	public void setInstalado(boolean instalado) {
		this.instalado = instalado;
	}

}
