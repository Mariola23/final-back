package es.cic.finalback.model;

import java.time.LocalDate;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "tipo")
public class Tipo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(unique = true)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer codigo;

	@Column(nullable = false)
	private String nombre;

	private String descripcion;

	@Column(nullable = false)
	private LocalDate fechaAlta;

	private LocalDate fechaModificacion;

	private boolean disponible;

	private boolean senial;

	@OneToMany(mappedBy = "tipo")
	private List<ElementoVial> elementos;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public LocalDate getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(LocalDate fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public LocalDate getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(LocalDate fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public boolean isDisponible() {
		return disponible;
	}

	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}

	public boolean isSenial() {
		return senial;
	}

	public void setSenial(boolean senial) {
		this.senial = senial;
	}

	public List<ElementoVial> getElementos() {
		return elementos;
	}

	public void setElementos(List<ElementoVial> elementos) {
		this.elementos = elementos;
	}
}
