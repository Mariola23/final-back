package es.cic.finalback.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.cic.finalback.assembler.AssemblerElementoVial;
import es.cic.finalback.dto.ElementoVialDTO;
import es.cic.finalback.dto.TipoDTO;
import es.cic.finalback.model.ElementoVial;
import es.cic.finalback.model.Tipo;
import es.cic.finalback.repository.ElementoVialRepository;
import es.cic.finalback.repository.TipoRepository;

@Service
@Transactional
public class ElementoVialService {

	@Autowired
	AssemblerElementoVial assembler;

	@Autowired
	ElementoVialRepository repo;
	
	@Autowired
	TipoRepository tipoRepo;

	public ElementoVialDTO crear(ElementoVialDTO dto) {
		ElementoVial t = assembler.toEntity(dto);
		repo.save(t);
		ElementoVialDTO result = assembler.toDto(t);
		return result;
	}

	@Transactional(readOnly = true)
	public List<ElementoVialDTO> list() {

		List<ElementoVial> tipos = this.repo.findAll();

		return assembler.toDto(tipos);

	}

	public ElementoVialDTO find(Long id) {

		ElementoVial elemento = repo.findById(id)
				.orElseThrow(() -> new UnsupportedOperationException("Elemento no encontrado"));
		return assembler.toDto(elemento);
	}

	public List<ElementoVialDTO> getElementosVialesPorTipo(Long idTipo) {
		List<ElementoVial> elementos =repo.findByTipoId(idTipo);
		return assembler.toDto(elementos);
	}

	public ElementoVialDTO update(Long id,ElementoVialDTO elementoModificado) {
		ElementoVial elementoActual = repo.findById(id).orElseThrow(() -> new UnsupportedOperationException("Tipo no encontrado"));
		
		elementoActual.setTipo(tipoRepo.findById(elementoModificado.getFk_tipo()).get());
		elementoActual.setLatitud(elementoModificado.getLatitud());
		elementoActual.setLongitud(elementoModificado.getLongitud());
		elementoActual.setAltitud(elementoModificado.getAltitud());
		if(elementoModificado.isInstalado()==false) {
			elementoActual.setFechaRetirada(LocalDate.now());
		}
		elementoActual.setInstalado(elementoModificado.isInstalado());

		repo.save(elementoActual);

		ElementoVialDTO resultado = assembler.toDto(elementoActual);

		return resultado;
	}
	
	public ElementoVialDTO retirar(Long id) {
		ElementoVial elementoActual = repo.findById(id).orElseThrow(() -> new UnsupportedOperationException("Tipo no encontrado"));
		
		elementoActual.setInstalado(false);

		repo.save(elementoActual);

		ElementoVialDTO resultado = assembler.toDto(elementoActual);

		return resultado;
	}

}
