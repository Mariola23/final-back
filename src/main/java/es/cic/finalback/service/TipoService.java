package es.cic.finalback.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.cic.finalback.assembler.AssemblerTipo;
import es.cic.finalback.dto.TipoDTO;
import es.cic.finalback.model.Tipo;
import es.cic.finalback.repository.TipoRepository;

@Service
@Transactional
public class TipoService {

	@Autowired
	AssemblerTipo assembler;

	@Autowired
	TipoRepository repo;

	public TipoDTO crear(TipoDTO dto) {
		
		dto.setFechaAlta(LocalDate.now());
		Tipo t = assembler.toEntity(dto);
		repo.save(t);
		TipoDTO result = assembler.toDto(t);
		return result;
	}

	@Transactional(readOnly = true)
	public List<TipoDTO> list() {

		List<Tipo> tipos = this.repo.findAll();

		return assembler.toDto(tipos);

	}

	public TipoDTO find(Long id) {

		Tipo tipo = repo.findById(id).orElseThrow(() -> new UnsupportedOperationException("Tipo no encontrado"));
		return assembler.toDto(tipo);
	}

	public TipoDTO update(Long id,TipoDTO tipoModificado) {
		Tipo tipoActual = repo.findById(id).orElseThrow(() -> new UnsupportedOperationException("Tipo no encontrado"));
		
		tipoActual.setNombre(tipoModificado.getNombre());
		tipoActual.setDescripcion(tipoModificado.getDescripcion());
		tipoActual.setDisponible(tipoModificado.isDisponible());
		tipoActual.setSenial(tipoModificado.isSenial());
		tipoActual.setFechaModificacion(LocalDate.now());

		repo.save(tipoActual);

		TipoDTO resultado = assembler.toDto(tipoActual);

		return resultado;
	}
	
	public TipoDTO deshabilitar(Long id) {
		Tipo tipoActual = repo.findById(id).orElseThrow(() -> new UnsupportedOperationException("Tipo no encontrado"));
		
		tipoActual.setDisponible(false);

		repo.save(tipoActual);

		TipoDTO resultado = assembler.toDto(tipoActual);

		return resultado;
	}
	
	public void delete(TipoDTO dto) {
		repo.findById(dto.getId()).orElseThrow(() -> new UnsupportedOperationException("Tipo no encontrado"));

		Tipo tipo = assembler.toEntity(dto);

		repo.delete(tipo);
	}

}
