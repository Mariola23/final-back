package es.cic.finalback.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import es.cic.finalback.dto.ElementoVialDTO;
import es.cic.finalback.dto.TipoDTO;
import es.cic.finalback.service.ElementoVialService;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/elemento")
@Validated
public class ElementoVialController {
	
	private static Logger logger = LogManager.getLogger(ElementoVialController.class);

	@Autowired
	private ElementoVialService serv;
	
	@PostMapping
	@ResponseStatus(value = HttpStatus.CREATED)
	public ElementoVialDTO create(@RequestBody @Valid ElementoVialDTO dto) {

		logger.info("Estas creando un elemento");

		return serv.crear(dto);
	}

	@GetMapping("/{id}")
	public ElementoVialDTO find(@PathVariable(name = "id") Long id) {
		logger.info(() -> String.format("Has consultado el elemento %d", id));
		return serv.find(id);
	}
	
	@GetMapping
	public List<ElementoVialDTO> readAll() {
		logger.info("Lista de elemento");
		return serv.list();
	}
	
	@GetMapping("/porTipo/{tipoId}")
    public List<ElementoVialDTO> findElementosPorTipo(@PathVariable(name = "tipoId") Long tipoId) {
		logger.info(() -> String.format("Has consultado los elemento con tipo: ", tipoId));
        return serv.getElementosVialesPorTipo(tipoId);
    }
	
	@PutMapping("/{id}")
	public ElementoVialDTO actualizarElemento(@PathVariable Long id, @RequestBody ElementoVialDTO elementoActualizado) {
		return serv.update(id, elementoActualizado);
	}
	
	@PutMapping("/{id}/retirar")
	public ElementoVialDTO retirarElemento(@PathVariable Long id) {
		return serv.retirar(id);
	}

}
