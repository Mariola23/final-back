package es.cic.finalback.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import es.cic.finalback.dto.TipoDTO;
import es.cic.finalback.service.TipoService;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/tipo")
@Validated
public class TipoController {

	private static Logger logger = LogManager.getLogger(TipoController.class);

	@Autowired
	private TipoService serv;

	@PostMapping
	@ResponseStatus(value = HttpStatus.CREATED)
	public TipoDTO create(@RequestBody @Valid TipoDTO dto) {

		logger.info("Estas creando un tipo");

		return serv.crear(dto);
	}

	@GetMapping("/{id}")
	public TipoDTO find(@PathVariable(name = "id") Long id) {
		logger.info(() -> String.format("Has consultado el tipo %d", id));
		return serv.find(id);
	}

	@GetMapping
	public List<TipoDTO> readAll() {
		logger.info("Lista de tipos");
		return serv.list();
	}

	@PutMapping("/{id}")
	public TipoDTO actualizarTipo(@PathVariable Long id, @RequestBody TipoDTO tipoActualizado) {
		return serv.update(id, tipoActualizado);
	}
	
	@PutMapping("/{id}/deshabilitar")
	public TipoDTO deshabilitarTipo(@PathVariable Long id) {
		return serv.deshabilitar(id);
	}

	@DeleteMapping("/{id}")
	public void eliminar(@PathVariable(name = "id") Long id) {
		logger.info(String.format("Estas eliminando el tipo %d", id));

		serv.delete(serv.find(id));

	}

}
