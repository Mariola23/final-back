package es.cic.finalback;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import es.cic.finalback.controller.ElementoVialController;
import es.cic.finalback.dto.ElementoVialDTO;
import es.cic.finalback.service.ElementoVialService;

@SpringBootTest
@AutoConfigureMockMvc
class ElementoIntegrationTest {

	@Mock
    private ElementoVialService elementoVialService;

    @InjectMocks
    private ElementoVialController elementoVialController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCrearElementoVial() {
        // Arrange
        ElementoVialDTO elementoVialDTO = new ElementoVialDTO();
        elementoVialDTO.setId(1L);
        elementoVialDTO.setFk_tipo(1L);
        elementoVialDTO.setLatitud(40.0F);
        elementoVialDTO.setLongitud(-3.0F);
        elementoVialDTO.setAltitud(100.0F);
        elementoVialDTO.setInstalado(true);

        when(elementoVialService.crear(elementoVialDTO)).thenReturn(elementoVialDTO);

        // Act
        ElementoVialDTO respuesta = elementoVialController.create(elementoVialDTO);

        // Assert
        assertNotNull(respuesta);
        assertEquals(elementoVialDTO, respuesta);
    }

    @Test
    public void testEncontrarElementoVialExistente() {
        // Arrange
        Long elementoVialId = 1L;
        ElementoVialDTO elementoVialDTO = new ElementoVialDTO();
        elementoVialDTO.setId(elementoVialId);

        when(elementoVialService.find(elementoVialId)).thenReturn(elementoVialDTO);

        // Act
        ElementoVialDTO respuesta = elementoVialController.find(elementoVialId);

        // Assert
        assertNotNull(respuesta);
        assertEquals(elementoVialDTO, respuesta);
    }

    @Test
    public void testEncontrarElementoVialNoExistente() {
        // Arrange
        Long elementoVialId = 1L;

        when(elementoVialService.find(elementoVialId)).thenReturn(null);

        // Act
        ElementoVialDTO respuesta = elementoVialController.find(elementoVialId);

        // Assert
        assertNull(respuesta);
    }

    @Test
    public void testListarElementosViales() {
        // Arrange
        ElementoVialDTO elementoVial1 = new ElementoVialDTO();
        elementoVial1.setId(1L);
        elementoVial1.setFk_tipo(1L);
        elementoVial1.setLatitud(40.0F);
        elementoVial1.setLongitud(-3.0F);
        elementoVial1.setAltitud(100.0F);
        elementoVial1.setInstalado(true);

        ElementoVialDTO elementoVial2 = new ElementoVialDTO();
        elementoVial2.setId(2L);
        elementoVial2.setFk_tipo(2L);
        elementoVial2.setLatitud(41.0F);
        elementoVial2.setLongitud(-4.0F);
        elementoVial2.setAltitud(90.0F);
        elementoVial2.setInstalado(false);

        List<ElementoVialDTO> elementosViales = new ArrayList<>();
        elementosViales.add(elementoVial1);
        elementosViales.add(elementoVial2);

        when(elementoVialService.list()).thenReturn(elementosViales);

        // Act
        List<ElementoVialDTO> respuesta = elementoVialController.readAll();

        // Assert
        assertNotNull(respuesta);
        assertEquals(elementosViales, respuesta);
    }

}
