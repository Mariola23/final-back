package es.cic.finalback;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import es.cic.finalback.controller.TipoController;
import es.cic.finalback.dto.TipoDTO;
import es.cic.finalback.service.TipoService;

@SpringBootTest
@AutoConfigureMockMvc
class TipoIntegrationTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private TipoController controller;
	
	@Autowired
	private TipoService service;

	private static TipoDTO tipo;

	@BeforeEach
	void setUp() throws Exception {
		tipo = new TipoDTO();
		tipo.setId(1L);
		tipo.setNombre("Semáforo");
		tipo.setDescripcion("de dos focos");
		tipo.setDisponible(true);
		tipo.setSenial(true);
	}

	@Test
	void testCrearTipo() throws Exception {
		String content = objectMapper.writeValueAsString(tipo);
		MvcResult result = mockMvc
				.perform(post("/api/tipo").contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
						.content(content.getBytes()))
				.andDo(print()).andExpect(status().isCreated()).andExpect(jsonPath("$.id").isNumber()).andReturn();
		String contentResult = result.getResponse().getContentAsString();

		tipo = objectMapper.readValue(contentResult, TipoDTO.class);

		// Verifica que se haya creado el id
		assertNotNull(tipo.getId());

		// Lo saca de la BD
		assertNotNull(controller.find(tipo.getId()));
	}

	@Test
	void testDeshabilitarTipo() throws Exception {
		
		service.crear(tipo);
		
		String content = objectMapper.writeValueAsString(tipo);

		MvcResult result = mockMvc
				.perform(MockMvcRequestBuilders.put("/api/tipo/{id}/deshabilitar", tipo.getId())
						.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
						.content(content.getBytes()))
				.andDo(print()).andExpect(status().isOk())
				.andReturn();
		String contentResult = result.getResponse().getContentAsString();

		tipo = objectMapper.readValue(contentResult, TipoDTO.class);

		// Verifica que se haya deshabilitado
		assertFalse(tipo.isDisponible());
	}

}
