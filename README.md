Controlar un sistema de elementos de tráfico.

Sistema de tráfico una pantalla donde se ve toda la ciudad y la ubicación de los distintos elementos de tráfico y su estado, por ejemplo: un semáforo en verde o en rojo.

#ONE to MANY
Hay elementos de tráfico y estos elementos son de un tipo en concreto.
Hay varios elementos del mismo tipo.

**CAMPOS**

#TIPO:
id,
código,
nombre,
descripción,
fechaAlta,
fechaModificacion (ultima fecha de cambio de disponibilidad),
disponible(disponible o no),
señal(si es una señal o no)

#ELEMENTO VIAL:
id,
código,
tipo_id,
latitud,
longitud,
altitud,
fechaInstalacion,
fechaRetirada,
instalado(boolean instalado o retirado)


Requisitos prioritarios:

#Listar/Dar de alta tipos
#Lista/Dar de alta elementos
